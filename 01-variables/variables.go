package main

import (
	"fmt"
)

func main() {
	// define a variable
	// you can read the next line like: we define a "var"iable with the name "mystring" of type "string"
	var mystring string
	// lets print the defined var which is NOT inizialized. What do you think, will be printed.
	fmt.Println(mystring)

	// New we assign a value to the variable and print it.
	mystring = "This is my string."
	fmt.Println(mystring)

	// we define and initialize a new variable in one step.
	// There is no difference to the first example except that it is done in one step.
	mynextstring := "This is my next string."
	fmt.Println(mynextstring)

	// And here is another way to define a variable and assign a value to it.
	var myint = 1
	fmt.Println(myint)

}
