package main

import (
	"fmt"
	"math"
)

type Circle struct {
	Radius float64
}

type Rectangle struct {
	X float64
	Y float64
}

func (c Circle) CalculateArea() float64 {
	return (math.Pow(c.Radius, 2) * math.Pi)
}

func (c Circle) CalculatePerimeter() float64 {
	return (c.Radius * 2 * math.Pi)
}

func (r *Rectangle) CalculateArea() float64 {
	return (r.X * r.Y)
}

func (r *Rectangle) CalculatePerimeter() float64 {
	return ((2 * r.X) + (2 * r.Y))
}

func (r *Rectangle) SetX(x float64) {
	r.X = x
}

func (r *Rectangle) SetY(y float64) {
	r.Y = y
}

func main() {
	circle := Circle{
		Radius: 5,
	}

	fmt.Printf("Circle with radius %f\n", circle.Radius)
	fmt.Printf(" - Area     : %f\n", circle.CalculateArea())
	fmt.Printf(" - Perimeter: %f\n", circle.CalculatePerimeter())

	rectangle := Rectangle{}
	// Rectangle.SetX has a pointer receiver, so we can set values on the instance
	rectangle.SetX(2)
	rectangle.SetY(3)

	fmt.Printf("Rectangle with X: %f Y: %f\n", rectangle.X, rectangle.Y)
	fmt.Printf(" - Area     : %f\n", rectangle.CalculateArea())
	fmt.Printf(" - Perimeter: %f\n", rectangle.CalculatePerimeter())

}
