package main

import (
	"fmt"

	"gitlab.com/sebidude/learn-go/08-packages/foobar"
)

func main() {
	// the import provides a gofile which hosts the package "bar"
	// with new we create an instance of bar.Bar and get the pointer to it.
	//favbar := new(bar.Bar)

	// we can also use our own "constructor"
	favbar := bar.NewBar("Sink & Drink")

	err := favbar.Enter()
	if err != nil {
		fmt.Println(err)
	}

	favbar.Open()
	if favbar.IsOpen() {
		fmt.Println("The bar is open now.")
		favbar.Enter()
		fmt.Printf("We are now in '%s'. %d guests present.\n", favbar, favbar.Guests())
	}

	for i := 0; i < 25; i++ {
		if i%3 == 0 {
			err := favbar.Leave()
			if err != nil {
				fmt.Println(err)
			}
		} else {
			err := favbar.Enter()
			if err != nil {
				fmt.Println(err)
			}
		}

	}
	fmt.Printf("There are %d guests present.\n", favbar.Guests())

}
