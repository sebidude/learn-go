package bar

import (
	"fmt"
)

type Bar struct {
	name   string
	isOpen bool
	guests int
}

func NewBar(name string) *Bar {
	return &Bar{
		name:   name,
		guests: 0,
		isOpen: false,
	}
}

func (b *Bar) String() string {
	return b.name
}

func (b *Bar) Open() {
	b.isOpen = true
}

func (b *Bar) Close() {
	b.isOpen = false
}

func (b *Bar) IsOpen() bool {
	return b.isOpen
}

func (b *Bar) Enter() error {
	if b.isOpen {
		b.guests++
		return nil
	} else {
		return fmt.Errorf("The bar is closed")
	}
}

func (b *Bar) Leave() error {
	if b.guests < 1 {
		return fmt.Errorf("No more guests.")
	}
	b.guests--
	return nil

}

func (b *Bar) Guests() int {
	return b.guests
}
