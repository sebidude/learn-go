package main

import (
	"bytes"
	"fmt"
	"text/template"
)

type Params struct {
	Name     string
	Kind     string
	Hostname []string
	IPs      []string
}

func main() {
	/*
		funcMap := template.FuncMap{
			// The name "inc" is what the function will be called in the template text.
			"inc": func(i int) int {
				return i + 1
			},
		}

		// this does not work. Find out why...
		t, err := template.New("config").Funcs(funcMap).ParseFiles("templates/config.tpl")
	*/
	t, err := template.ParseFiles("templates/config.tpl")
	if err != nil {
		fmt.Println(err)
	}

	params := &Params{
		Name:     "dns-1",
		Kind:     "important",
		Hostname: []string{"dns-1.bla", "dns-2.bla"},
		IPs:      []string{"1.1.1.1", "1.1.1.2"},
	}

	var tpl bytes.Buffer

	t.Execute(&tpl, params)

	fmt.Printf("%#v\n", tpl)
	fmt.Printf("%#v\n", params)

	fmt.Println(tpl.String())

}
