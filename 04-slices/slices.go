package main

import (
	"fmt"
)

func main() {

	// we create a slice
	myints := make([]int, 10)
	for i := 0; i < len(myints); i++ {
		myints[i] = i
	}
	fmt.Printf("%#v\n", myints)

	// Check this page: https://github.com/golang/go/wiki/SliceTricks
}
