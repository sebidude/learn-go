package main

import (
	"fmt"
	"os"

	"gopkg.in/alecthomas/kingpin.v2"
)

func addSubCommand(app *kingpin.Application, name string, description string) {
	app.Command(name, description).Action(func(c *kingpin.ParseContext) error {
		fmt.Printf("Would have run command %s.\n", name)
		return nil
	})
}

func main() {
	app := kingpin.New("kingpin", "My Sample Kingpin App!")
	app.Flag("flag-1", "").String()
	app.Flag("flag-2", "").HintOptions("opt1", "opt2").String()

	configureNetcatCommand(app)

	// Add some additional top level commands
	addSubCommand(app, "ls", "Additional top level command to show command completion")
	addSubCommand(app, "ping", "Additional top level command to show command completion")
	addSubCommand(app, "nmap", "Additional top level command to show command completion")

	kingpin.MustParse(app.Parse(os.Args[1:]))
}

type NetcatCommand struct {
	hostName string
	port     int
	format   string
}

func (n *NetcatCommand) run(c *kingpin.ParseContext) error {
	fmt.Printf("Would have run netcat to hostname %v, port %d, and output format %v\n", n.hostName, n.port, n.format)
	return nil
}

func configureNetcatCommand(app *kingpin.Application) {
	c := &NetcatCommand{}
	nc := app.Command("nc", "Connect to a Host").Action(c.run)
	nc.Flag("nop-flag", "Example of a flag with no options").Bool()
}
