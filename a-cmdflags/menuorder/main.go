package main

import (
	"os"

	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	flag1 string
	flag2 string
)

func main() {
	app := kingpin.New(os.Args[0], "My Sample Kingpin App!")
	//app.UsageTemplate(kingpin.DefaultUsageTemplate)
	app.Flag("drink", "Which drink?").Short('f').HintOptions("water", "beer").StringVar(&flag1)
	app.Flag("food", "Which food?").Short('b').HintOptions("pizza", "burger").StringVar(&flag2)

	app.Parse(os.Args[1:])
}
