package main

import (
	"fmt"
)

type MyEmtpyStruct struct{}

type MyStruct struct {
	Name  string
	Count int
}

type MySecondStruct struct {
	Name  string
	Count int
}

var (
	mystruct MyStruct
)

func main() {

	fmt.Printf("%#v\n", mystruct)
	mystruct.Name = "I can haz name."
	PrintMyStruct()
	fmt.Println("-----------------------")

	var myemptystruct MyEmtpyStruct
	fmt.Printf("%#v\n", myemptystruct)
	fmt.Println("-----------------------")

	mysecondstruct := MySecondStruct{}
	fmt.Printf("%#v\n", mysecondstruct)
	fmt.Println("-----------------------")

	myfilledstruct := MyStruct{
		Name:  "myfilledstruct",
		Count: 1337,
	}
	fmt.Printf("%#v\n", myfilledstruct)
	fmt.Println("-----------------------")

	myfilledstruct.Count = 9001
	myfilledstruct.Name = "Over9000"
	fmt.Printf("%#v\n", myfilledstruct)
	fmt.Println("-----------------------")

	PrintStruct(myfilledstruct)
	fmt.Println("-----------------------")

	// we pass a value
	WontChangeStruct(myfilledstruct)
	fmt.Printf("%#v\n", myfilledstruct)
	fmt.Println("-----------------------")

	// we pass a pointer
	WillChangeStruct(&myfilledstruct)
	fmt.Printf("%#v\n", myfilledstruct)
	fmt.Println("-----------------------")
}

func PrintMyStruct() {
	fmt.Printf("%#v\n", mystruct)
}

func PrintStruct(s MyStruct) {
	fmt.Printf("%#v\n", s)
}

func WontChangeStruct(s MyStruct) {
	s.Name = "No new name."
}

func WillChangeStruct(s *MyStruct) {
	s.Name = "New Name."
}
