package main

import (
	"fmt"
)

var (
	myglobalslice []int
)

func main() {
	// this will panic. Uncomment to test.
	// you cannot access the key of a slice like this.
	//myglobalslice[0] = 1
	//fmt.Printf("%#v", myglobalslice)

	// this will work
	myglobalslice = append(myglobalslice, 1)
	fmt.Printf("%#v\n", myglobalslice)

	// this will not work. Uncomment to test.
	//myglobalslice = append(myglobalslice, {1, 2, 3, 4})
	//fmt.Printf("%#v\n", myglobalslice)

	// append a slice to a slice
	myappendstuff := []int{1, 2, 3, 4, 5}
	myglobalslice = append(myglobalslice, myappendstuff...)
	fmt.Printf("%#v\n", myglobalslice)

	myslice := append([]string{"Say", "hello"}, []string{"to", "your", "appends"}...)
	fmt.Printf("%#v\n", myslice)

	// you cannot mix types when the slice is typed. Uncomment to test
	//mytypeslice := append([]string{"one", "two"}, []int{1, 2}...)
	//fmt.Printf("%#v\n", mytypeslice)

	// use make to initialize a slice of a certain capacity.
	mymakeslice := make([]int, 10)
	fmt.Printf("%#v\n", mymakeslice)
	mymakeslice[1] = 1
	fmt.Printf("%#v\n", mymakeslice)

	myinitslice := make([]int, 0, 10)
	fmt.Printf("%#v\n", myinitslice)
	// this will panic
	//myinitslice[0] = 1
	myinitslice = append(myinitslice, 1)
	fmt.Printf("%#v\n", myinitslice)

	// loop over a slice an empty slice will not loop.
	myloopslice := make([]int, 0, 10)
	for key, value := range myloopslice {
		fmt.Println(key, value)
	}

	myloopslice = make([]int, 10)
	for key, value := range myloopslice {
		fmt.Printf("key: %d => value: %d\n", key, value)
	}

	// capacity is len of the slice
	for i := 0; i < len(myloopslice); i++ {
		myloopslice[i] = i
	}
	fmt.Printf("%#v\n", myloopslice)

	for i := 0; i < 20; i++ {
		// this will panic at runtime because the size of myloopslice is only 10
		myloopslice[i] = i
	}
	fmt.Printf("%#v\n", myloopslice)

}
