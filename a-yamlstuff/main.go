package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/ghodss/yaml"
)

type MyObject struct {
	Name   string       `json:"name"`
	Path   string       `json:"path"`
	Config ConfigObject `json:"config"`
}

type ConfigObject struct {
	MaxAge int    `json:"maxage"`
	Tier   string `json:"tier"`
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("%s <filename>\n", os.Args[0])
		os.Exit(1)
	}
	yamlFileName := os.Args[1]

	yamlFileContent, err := ioutil.ReadFile(yamlFileName)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	var myobj MyObject
	err = yaml.Unmarshal(yamlFileContent, &myobj)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	fmt.Printf("Object: %s\n", myobj.Name)
	fmt.Printf(" Path: %s\n", myobj.Path)
	fmt.Println(" Config:")
	fmt.Printf("  MaxAge: %d\n", myobj.Config.MaxAge)
	fmt.Printf("  Tier: %s\n", myobj.Config.Tier)
}
