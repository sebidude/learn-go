package main

import (
	"fmt"
)

var (
	// these variables are global to the main package.
	mynextstring = "This is my next string."
	myint        int
	mylittleint  = 5
	mybigint     int
)

func main() {
	var mystring string
	fmt.Println(mystring)
	mystring = "This is my string."
	fmt.Println(mystring)

	// mynextstring is global to main.
	fmt.Println(mynextstring)

	myint = 1
	fmt.Println(myint)

	// mylittleint is globally defined so we can print it.
	fmt.Println(mylittleint)
	var mylittleint = 10
	fmt.Println(mylittleint)

	fmt.Println(mybigint)
	mybigint := 1024
	fmt.Println(mybigint)

	// we cannot use this outside the main goroutine as it is not defined globally
	mytestvar := "test me"
	fmt.Println(mytestvar)

	PrintMe()

}

func PrintMe() {
	fmt.Println(mynextstring)

	// this will not work. Uncomment and try to compile it.
	//fmt.Println(mytestvar)
}
