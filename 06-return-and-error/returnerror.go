package main

import (
	"fmt"
)

func GetString() string {
	return "A string"
}

func GetErrorAndString(input string) (string, error) {
	if input == "error" {
		// this will not work. we cannot use nil as type string
		//return nil, fmt.Errorf("Return error")

		// but this will work
		return "", fmt.Errorf("Return error")
	}
	return "No error.", nil
}

func GetErrorOrString(input string) (*string, error) {
	if input == "error" {
		// hey we can return nil pointer...
		return nil, fmt.Errorf("Return error")
	}
	returnstring := "No error"
	return &returnstring, nil
}

func main() {
	fmt.Println(GetString())

	mystring, err := GetErrorAndString("test")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(mystring)

	mystring, err = GetErrorAndString("error")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(mystring)

	mystringpointer, err := GetErrorOrString("test")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(mystringpointer)

	mystringpointer, err = GetErrorOrString("error")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(mystringpointer)
	// one does not simply access a nil pointer
	fmt.Println(*mystringpointer)

}
